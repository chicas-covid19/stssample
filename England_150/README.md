Sample Data Sets
================

This repository contains sample data sets for the `stsmodel` package.

England 150
===========

The map contains 150 artificially constructed and named regions covering
England. The population of each area is also random.

Traffic rates and flows are a uniform random distribution.

The case line listing is generated from a simple exponential decay model,
except for four regions, Soutngton, Northton, Greatone, and Eastford, which
have had an artificial increase in their cases applied for the most recent 12 days.

Most of the other columns in the line listing apart from `specimen_date`,
`LTLA_code`, and `LTLA_name` (including age, sex, and onset date) are junk
and not useful. They only exist so that the XLSX file mimics the actual one
used in the production service.

Fitting the Model
=================

With the `stsmodel` package attached you can run the model on this data. With
the `stsmodel` package (available separately) and from a working directory
above this repository run:

```
run_from("./stssample/England_150/")
```

This should create the outputs in a `2020-08-01` folder. If you re-run
this note that the system stores data checksums for all the files, and
if they don't change then the analysis doesn't bother running again. If you
wish to re-run then delete the `2020-08-01` folder and start again.


Viewing The Results
===================

Opening a web browser at the `2020-08-01` folder should result in a map
of model cases and flags for significant increasing and decreasing (over
the model predictions) areas. The boosted areas listed are shown in red
on `boosted.jpg` in the folder.

The `Map-2020-08-01` folder contains sample output which should be the
same as any other created by `stsmodel` on this data apart from any
monte-carlo difference.


